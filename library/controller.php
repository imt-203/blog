<?php
$action = empty($_GET['action']) ? 'home' : $_GET['action'];
$page = null;
$title = "Мой супер сайт";

switch ($action) {
	case 'home':
		require_once('./actions/home.php');
		break;
	case 'about':
		require_once('./actions/about.php');
		break;
	case 'new':
		require_once('./actions/new.php');
		break;
	case 'shop':
		require_once('./actions/shop.php');
		break;
	case 'cart':
		require_once('./actions/cart.php');
		break;
	case 'edit':
		require_once('./actions/update.php');
		break;
	default:
		require_once('./actions/404.php');
		break;
}