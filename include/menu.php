<!-- Fixed navbar -->
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
  <a class="navbar-brand" href="/">Main</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item <?=($action == 'home') ? 'active' : '' ?>">
        <a class="nav-link" href="/index.php?action=home">Главная<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item <?=($action == 'about') ? 'active' : '' ?>">
        <a class="nav-link" href="/index.php?action=about">О нас</a>
      </li>
      <li class="nav-item <?=($action == 'new') ? 'active' : '' ?>">
        <a class="nav-link" href="/index.php?action=new">Создать</a>
      </li>
      <li class="nav-item <?=($action == 'shop') ? 'active' : '' ?>">
        <a class="nav-link" href="/index.php?action=shop">Магазин</a>
      </li>
    </ul>
    <a href="/index.php?action=cart"><i class="fas fa-shopping-cart"></i></a>
  </div>
</nav>